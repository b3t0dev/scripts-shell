#!/bin/bash

echo -e "\... Redirecionadores do Linux ... \n"

echo -e "Existe alguns tipos de redirecionadores de saida do Linux... \n Segue alguns exemplos:"

echo '
>  - Redireciona a saida (stdout) para um arquivo
>> - Redireciona a saida (stdout) mantendo o conteudo no arquivo sem apagar o conteudo existente.
2> - Redireciona a saida de erro (stderr) para algum destino
<  - Redireciona para a entrada'
echo 
echo 'Exemplo 1: pwd > diretorio_atual'
pwd > diretorio_atual
sleep 2

echo 'Exemplo 2: ls >> diretorio_atual - Incrementando no arquivo as informacoes de ls'
ls >> diretorio_atual
sleep 2

echo 'Exemplo 3: ls /root 2>> diretorio_atual - Caso nao seja root, o erro de permissao sera adicionado em diretorio_atual'
ls /root 2>> diretorio_atual
sleep 2

echo "Exemplo 4: cat < $0 - Ira exibir o conteudo deste script"
sleep 3
cat < $0
