#escreva um script que este menu tenha 3 opções
#1 - Exibir todos os users do sistema
#2 - Exibir IP da maquina
#3 - Sair

#!/bin/bash

function menu(){

    echo "\n ..:: MENU ::.. \n"
    echo "(1) - Exibir todos os usuários do sistema "
    echo "(2) - Exibir IP da Maquina "
    echo "(3) - Sair \n"
    
}

while [ "opc" != "3" ]; do
    menu
    read -p "\n Escolha uma opção: " opc

    case $opc in
    "1") cat /etc/passwd | cut -d ":" -f "1" ;;
    "2") hostname -I ;;
    "3") break ;;
    esac
done

