#!/bin/bash
> TextoOrd
for distro in $(cut -d ' ' -f 3 texto | sort | uniq ); do
	echo $distro >> TextoOrd
done

for linha in $(cat TextoOrd); do
	cont=0
	for distr in $(cut -d " " -f 3 texto); do
		if [ "$distr" == "$linha" ]; then
			cont=$(( cont+1 ))
		fi
	done
	echo "$linha = $cont"
done

rm TextoOrd
