#!/bin/bash


echo '===== Exemplos de Variaveis Automaticas ======'

echo '$# - Retorna o número de argumentos que o programa recebeu.'
echo "$#"
sleep 2

echo '$$ - Fornece o PID do processo do Shell.'
echo "$$"
sleep 2

echo '$! - Fornece o PID do último programa em execução em segundo plano.'
echo "$!"
sleep 2

echo '$0 - Retorna o nome do programa executado.'
echo "$0"
sleep 2

echo '$* - Retorna todos os argumentos informados na execução do programa.'
echo "$*"
sleep 2

echo '$USER - Exibe o nome do usuário logado no shell'
echo "$USER"
sleep 2

echo '$PWD - Exibe o diretório atual, no caso onde o script esta sendo executado'
echo "$PWD"
sleep 2

echo '$HOME - Exibe o diretório do usuário logado'
echo "$HOME"
sleep 2

echo '$SHELL - Exibe o shell utilizado'
echo "$SHELL"
sleep 2

echo '$OSTYPE	- Exibe o tipo do sistema operacional'
echo "$OSTYPE"
sleep 2

echo '$RANDOM	- Gera um numero randômico e exibe na tela'
echo "$RANDOM"
sleep 2

echo '$UID - Exibe o User ID de quem executou o script'
echo "$UID"
sleep 2

echo '$PATH - Path do sistema atual'
echo "$PATH"
sleep 2

echo '$LANG - Exibe o idioma que o sistema esta usando'
echo "$LANG"
sleep 2

echo '$N(1,2,3..) - Exibe o parametro da posicao N'
echo "$1"
sleep 2

