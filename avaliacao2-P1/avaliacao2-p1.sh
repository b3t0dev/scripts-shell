#!/bin/bash

while true; do
	clear
	echo -e '\n -------- MENU -------- \n'	
	echo '- Digite [info] para verificar as informações do Hardware/Sistema'
	echo '- Digite [arq] para verificar se o arquivo existe'
	echo '- Digite [dir] para exibir o diretorio atual' 
	echo '- Digite [lsdir] para exibir os arquivos do diretorio atual'
	echo '- Digite [chdir] para alterar o diretorio atual'
	echo '- Digite [SAIR] para encerrar o MENU'
	echo

	read -p "Digite uma opção: " opc

	if [ $opc = "SAIR" ]; then
		break
	elif [ $opc = "info" ]; then

		echo -e "\n ----  Exibindo informações do Hardware... ---- \n"
		echo $(cat /proc/cpuinfo | head -5)
		sleep 2
		echo -e "\n ----  Exibindo informações de Memoria... ---- \n"
		echo $(free -m)
		sleep 2
		echo -e "\n ----  Exibindo informações sobre a Distribuição... ---- \n"
		echo $(lsb_release -a)
		sleep 4

	elif [ $opc = "arq" ]; then
		read -p "Digite o caminho/nomedoarquivo: " arqv
		if test -f $arqv ; then
			echo "Arquivo existe"
		else
			echo "Arquivo não existe"
		fi
		sleep 2
	elif [ $opc = "dir" ]; then
		echo "Exibindo o diretorio atual..."
		sleep 2
		pwd
		sleep 2
	elif [ $opc = "lsdir" ]; then
		echo "Exibindo os arquivos do diretorio atual..."
		sleep 2
		ls
		sleep 2
	elif [ $opc = "chdir" ]; then
		read -p "Digite o caminho para o novo diretorio: " caminho
		cd $caminho
		echo "Diretorio atual: $(pwd)"
		sleep 2
	fi
done
