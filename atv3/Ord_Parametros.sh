#!/bin/bash

echo "Numeros informados: $*"
echo "Numeros salvos em /tmp/numeros"

echo $1 > /tmp/numeros
echo $2 >> /tmp/numeros
echo $3 >> /tmp/numeros
echo $4 >> /tmp/numeros
echo $5 >> /tmp/numeros

sort -n /tmp/numeros
