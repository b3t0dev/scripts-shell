# 1 - Escreva um script que (ESQ) gere N linhas de Blablbla. N é um Are de Linha de Comando (ALC)
# 2 - ESQ receba o nome de um arquivo como "ALC" e inverta suas lnhas e critpgrafe-o usando "CIFRA DE CESAR"
# 3 - O mesmo do 2 mas Descriptografando
# 4 - ESQ exiba um mMenu com as seguintes OPções (que devem funcionar)
    # a - Selecionar um arquivo
    # b - Fazer um preview do arquivo Selecionado
    # c - Exibir um arquivo Selecionado
    # d - Criar um arquivo com N linhas de Blablabla
    # e - Pesquisar algo no arquivo Selecionado
    # f - Criar uma cópia de um arq selecionado
    # g - Sair

#!/bin/bash

function menu(){
    
    clear
    echo ""
    echo " ..:: MENU ::.. "
    echo ""
    echo "(a) Selecionar Arquivo"
    echo "(b) Fazer um preview do arquivo selecionado"
    echo "(c) Exibir um arquivo selecionado"
    echo "(d) Criar um arquivo com N linhas de blablabla"
    echo "(e) Pesquisar algo no arquivo selecionado"
    echo "(f) Criar uma cópia do arquivo selecionado"
    echo "(h) Criptografar Arquivo usando a Cript de Cesar"
    echo "(i) Descriptografar Arquivo"
    echo "(j) Sair"
    echo ""

}
arquivo=''
while [ "opc" != "g" ]; do
    menu
    read -p "Escoha uma opção: " opc
    echo ""

    case $opc in
    "a")
        read -p "Digite o caminho/nome do arquivo: " arq 
        if [ -f $arq ]; then
            arquivo=$arq
            echo ""
            echo "Aquivo selecionado com sucesso!"
            sleep 2

        else
            echo "Arquivo não existe"
            arquivo=''
            sleep 2

        fi
    ;;
    "b")
        if [ "$arquivo" != '' ]; then
            sleep 2
            clear
            echo "Exibindo resumo do arquivo"
            sleep 2
            echo "3 Primeiras linhas..."
            echo ""
            sleep 2
            head -3 $arquivo
            sleep 2
            clear
            echo "3 Ultimas linhas..."
            echo ""
            sleep 2
            tail -3 $arquivo
            sleep 2
            clear

        else
            echo "Arquivo não selecionado"
            sleep 3
            clear

        fi
    ;;
    "c") 
        sleep 2
        clear
        echo "Exibindo o arquivo selecionado"
        sleep 2
        cat $arquivo
        sleep 3
    ;;
    "d")
        read -p "Digite quantas linhas deseja criar o arquivo: " linhas
        ./blabla.sh $linhas
        sleep 2
        [ -f blablabla ] && echo "Arquivo criado com sucesso!" || echo "Arquivo foi criado!"
        sleep 2
        clear
        echo "Exibindo arquivo com $linhas linhas criado!!"
        echo ""
        sleep 3
        cat blablabla
        sleep 5
        clear
        
    ;;
    "e") 
        read -p "Digite a palavra que deseja pesquisar: " palavra
        if [ "$arquivo" != '' ]; then
            cat $arquivo | grep $palavra
        else
            echo "Arquivo não selecionado!"
        fi
        sleep 2

    ;;
    "f")
        if [ "$arquivo" != '' ]; then
            echo "Criando uma copia do arquivo selecionado"
            cp $arquivo _$arquivo
            sleep 2
            echo "Copia criada com sucesso. Copia: _$arquivo"
        else   
            echo "Arquivo não selecionado"
        fi
        sleep 2
    ;;
    "h")
        if [ "$arquivo" != '' ]; then
            echo "Criptografando arquivo (Critografia de cesar)"
            ./criptocesar.sh $arquivo
            sleep 2
            echo "Criptografia feita com sucesso!"
        else   
            echo "Arquivo não selecionado"
        fi
        sleep 2
    ;;
    "i")
        if [ "$arquivo" != '' ]; then
            echo "Descriptografando arquivo (Critografia de cesar)"
            ./descriptocesar $arquivo
            sleep 2
            echo "Descrriptografia feita com sucesso!"
        else   
            echo "Arquivo não selecionado"
        fi
        sleep 2 
    ;;
    "j") break ;;

    esac
done